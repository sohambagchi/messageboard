from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse
from ..models import Category, Post, Topic
from ..views import reply_topic

class ReplyTopicTestCase(TestCase):
    '''
    Base test case to be used in all `reply_topic` view tests
    '''
    def setUp(self):
        self.board = Category.objects.create(name='Django', description='Django board.')
        self.username = 'john'
        self.password = '123'
        user = User.objects.create_user(username=self.username, email='john@doe.com', password=self.password)
        self.topic = Topic.objects.create(subject='Hello, world', cat_g=self.board, starter=user)
        Post.objects.create(message='Lorem ipsum dolor sit amet', topic=self.topic, created_by=user)
        self.url = reverse('reply_topic', kwargs={'pk': self.cat_g.pk, 'topic_pk': self.topic.pk})

#class LoginRequiredReplyTopicTests(ReplyTopicTestCase):
    # ...

#class ReplyTopicTests(ReplyTopicTestCase):
    # ...

#class SuccessfulReplyTopicTests(ReplyTopicTestCase):
    # ...

#class InvalidReplyTopicTests(ReplyTopicTestCase):
    # ...
