# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-06-14 13:10
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('boards', '0002_auto_20190605_1050'),
    ]

    operations = [
        migrations.AlterField(
            model_name='topic',
            name='starter',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='topics', to=settings.AUTH_USER_MODEL),
        ),
    ]
