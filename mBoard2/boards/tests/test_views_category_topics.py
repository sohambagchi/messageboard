from django.test import TestCase
from django.urls import resolve
from django.core.urlresolvers import reverse
from ..views import home, cat_topics, new_topic
from ..models import Category, Topic, Post
from ..forms import NewTopicForm
from django.contrib.auth.models import User


class CategoryTopicsTests(TestCase):
    def setUp(self):
        Category.objects.create(name='Django', description='Django board.')

    def test_cat_topics_view_success_status_code(self):
        url = reverse('cat_topics', kwargs={'tp_pk': 1})
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)

    def test_cat_topics_view_not_found_status_code(self):
        url = reverse('cat_topics', kwargs={'tp_pk': 99})
        response = self.client.get(url)
        self.assertEquals(response.status_code, 404)

    def test_cat_topics_url_resolves_cat_topics_view(self):
        view = resolve('/message-board/categories/1/')
        self.assertEquals(view.func, cat_topics)

    def test_cat_topics_view_contains_navigation_links(self):
        cat_topics_url = reverse('cat_topics', kwargs={'tp_pk': 1})
        homepage_url = reverse('home')
        new_topic_url = reverse('new_topic', kwargs={'tp_pk': 1})
        response = self.client.get(cat_topics_url)
        self.assertContains(response, 'href="{0}"'.format(homepage_url))
        self.assertContains(response, 'href="{0}"'.format(new_topic_url))
