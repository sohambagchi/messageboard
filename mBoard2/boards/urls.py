from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings

from . import views

urlpatterns = [
    url(r'^categories/$', views.CategoryListView.as_view(), name='home'),
    url(r'^categories/(?P<tp_pk>\d+)/new/$', views.new_topic, name='new_topic'),
    url(r'^categories/(?P<tp_pk>\d+)/$', views.TopicListView.as_view(), name='cat_topics'),
    url(r'^categories/(?P<tp_pk>\d+)/topics/(?P<top_pk>\d+)/$', views.PostListView.as_view(), name='topic_posts'),
    url(r'^categories/(?P<tp_pk>\d+)/topics/(?P<top_pk>\d+)/reply/$', views.reply_topic, name='reply_topic'),
    url(r'^categories/(?P<tp_pk>\d+)/topics/(?P<top_pk>\d+)/posts/(?P<po_pk>\d+)/edit/$', views.PostUpdateView.as_view(), name='edit_post'),
]
