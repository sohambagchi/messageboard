# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-07-01 08:23
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('boards', '0010_auto_20190701_1346'),
    ]

    operations = [
        migrations.AddField(
            model_name='topic',
            name='doc',
            field=models.FileField(default='null', upload_to='documents/%Y/%m/%d/'),
        ),
    ]
