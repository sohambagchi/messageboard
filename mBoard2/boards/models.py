import math
from django.db import models
from django.contrib.auth.models import User
from django.utils.text import Truncator
# Create your models here.
class Category(models.Model):
    name = models.CharField(max_length=30, unique=True)
    description = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    def get_posts_count(self):
        return Post.objects.filter(topic__cat_g=self).count()

    def get_last_post(self):
        return Post.objects.filter(topic__cat_g=self).order_by('-created_at').first()

class Topic(models.Model):
    subject = models.CharField(max_length=255)
    last_updated = models.DateTimeField(auto_now_add=True)
    cat_g = models.ForeignKey(Category, related_name = 'topics')
    starter = models.ForeignKey(User, related_name = 'topics', null=True)
    views = models.PositiveIntegerField(default=0)
    doc = models.FileField(upload_to='documents/%Y/%m/%d/', blank=True, null=True)

    def __str__(self):
        return self.subject

    def get_page_count(self):
        count = self.posts.count()
        pages = count / 20
        return math.ceil(pages)

    def has_many_pages(self, count=None):
        if count is None:
            count = self.get_page_count()
        return count > 6

    def get_page_range(self):
        count = self.get_page_count()
        if self.has_many_pages(count):
            return range(1, 5)
        return range(1, count + 1)

    def get_last_ten_posts(self):
        return self.posts.order_by('-created_at')[:10]

    def doc_url(self):
        if self.doc and hasattr(self.doc, 'url'):
            return self.doc.url

class Post(models.Model):
    message = models.TextField(max_length=4000)
    topic = models.ForeignKey(Topic, related_name='posts')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(null=True)
    created_by = models.ForeignKey(User, null=True, related_name='posts')
    updated_by = models.ForeignKey(User, null=True, related_name='+')
    # docs = models.FileField(upload_to='documents/%Y/%m/%d/', default='null')
    docs = models.FileField(upload_to='documents/%Y/%m/%d/', null=True)

    def __str__(self):
        truncated_message = Truncator(self.message)
        return truncated_message.chars(30)
