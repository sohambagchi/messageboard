from django.test import TestCase
from django.urls import resolve
from django.core.urlresolvers import reverse
from ..views import home, cat_topics, new_topic, BoardListView
from ..models import Category, Topic, Post
from ..forms import NewTopicForm
from django.contrib.auth.models import User

# Create your tests here.

class HomeTests(TestCase):

    def setUp(self):
        self.cats = Category.objects.create(name='JS', description='About JS')
        url = reverse('home')
        self.response = self.client.get(url)

    def test_home_view_status_code(self):
        self.assertEquals(self.response.status_code, 200)

    def test_home_url_resolves_home_view(self):
        view = resolve('/message-board/categories/')
        self.assertEquals(view.func.view_class, BoardListView)

    def test_home_view_contains_link_to_topics_page(self):
        cat_topics_url = reverse('cat_topics', kwargs={'tp_pk': self.cats.pk})
        self.assertContains(self.response, 'href="{0}"'.format(cat_topics_url))
