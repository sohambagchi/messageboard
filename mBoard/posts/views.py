from django.contrib.auth.models import User
from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, Http404
from .models import Board, Topic, Post
from .forms import NewTopicForm

# Create your views here.

def boards(request):
    boards = Board.objects.all()
    return render(request, 'home.html', {'boards': boards})
    # boards_names = list()
    # for board in boards:
    # boards_names.append(board.name)
    # response_html = '<br>'.join(boards_names)
    # return HttpResponse(response_html)

def topics(request, pk):
    board = get_object_or_404(Board, pk=pk)
    return render(request, 'topics.html', {'board': board})
    # topics = Topic.objects.all()
    # topics_names = list()
    # for topic in topics:
    # topics_names.append(topic.subject)
    # response_html = '<br>'.join(topics_names)
    # try:
        # board = Board.objects.get(pk=pk)
    # except Board.DoesNotExist:
    # raise Http404
    #return HttpResponse(response_html)

def new_topic(request, pk):
    board = get_object_or_404(Board, pk=pk)
    #print('am board',board);
    user = User.objects.first()

    if request.method == 'POST':
        form = NewTopicForm(request.POST)
        if form.is_valid():
            topic = form.save(commit=False)
            topic.board = board
            topic.starter = user
            topic.save()

            # subject = request.POST['subject']
            # message = request.POST['message']

            # user = User.objects.first()

            # topic = Topic.objects.create(
            #    subject=subject,
            #    board=board,
            #    starter=user
            # )

            post = Post.objects.create(
                message=form.cleaned_data.get('message'),
                topic=topic,
                created_by=user,
            )

        return redirect('topics', pk=pk)
    else:
        form = NewTopicForm()
    # return render(request, 'new_topic.html', {'board': board,'pk':pk})
    return render(request, 'new_topic.html', {'board': board, 'form': form})

def posts(request):
    posts = Post.objects.all()
    posts_names = list()

    for post in posts:
        post_names.append(post.message[:50])

    response_html = '<br>'.join(posts_names)

    return HttpResponse(response_html)
