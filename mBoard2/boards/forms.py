from django import forms
from .models import Topic, Post
# from django.core.models import Document

class NewTopicForm(forms.ModelForm):
    message = forms.CharField(
        widget=forms.Textarea(
            attrs={'rows': 5, 'placeholder': 'What is on your mind?'}
        ),
        max_length=4000,
        help_text='The max length of the text is 4000'
    )
    docs = forms.FileField()
    def __init__(self, *args, **kwargs):
        super(NewTopicForm, self).__init__(*args, **kwargs)
        self.fields['docs'].required = False

    class Meta:
        model = Topic
        fields = ['subject', 'message', 'docs', ]

class PostForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(PostForm, self).__init__(*args, **kwargs)
        self.fields['docs'].required = False
    class Meta:
        model = Post
        fields = ['message', 'docs', ]
