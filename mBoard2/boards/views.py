from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.views.generic import UpdateView, ListView
from django.utils.decorators import method_decorator
from django.contrib.auth.models import User
from .models import Category, Topic, Post
from .forms import NewTopicForm, PostForm
from django.http import HttpResponse
from django.db.models import Count
from django.utils import timezone
# Create your views here.

def home(request):
    cats = Category.objects.all()
    return render(request, 'home.html', {'cats': cats})
    #return HttpResponse('Hello world')

def cat_topics(request, tp_pk):
    cats = get_object_or_404(Category, pk=tp_pk)
    queryset = cats.topics.order_by('-last_updated').annotate(replies=Count('posts') - 1)
    page = request.GET.get('page', 1)

    paginator = Paginator(queryset, 20)

    try:
        topics = paginator.page(page)
    except PageNotAnInteger:
        # go back to first Page
        topics = paginator.page(1)
    except EmptyPage:
        # for invalid numbers (typos); go to last Page
        topics = paginator.page(paginator.num_pages)

    return render(request, 'topics.html', {'cats': cats, 'topics': topics})

@login_required
def new_topic(request, tp_pk):
    cats = get_object_or_404(Category, pk=tp_pk)
    # user = User.objects.first()
    if request.method == 'POST':
        form = NewTopicForm(request.POST, request.FILES)
        if form.is_valid():
            topic = form.save(commit=False)
            topic.cat_g = cats
            topic.starter = request.user
            topic.save()
            post = Post.objects.create(
                message=form.cleaned_data.get('message'),
                docs=form.cleaned_data.get('docs'),
                topic=topic,
                created_by=request.user,
            )
            return redirect('topic_posts', tp_pk=cats.pk, top_pk=topic.pk)
    else:
        form = NewTopicForm()
    return render(request, 'new_topic.html', {'cats': cats, 'form': form})

def topic_posts(request, tp_pk, top_pk):
        topic = get_object_or_404(Topic, cat_g__pk=tp_pk, pk=top_pk)
        topic.views += 1
        topic.save()
        return render(request, 'topic_posts.html', {'topic': topic})

@login_required
def reply_topic(request, tp_pk, top_pk):
    topic = get_object_or_404(Topic, cat_g__pk=tp_pk, pk=top_pk)
    if request.method == 'POST':
        form = PostForm(request.POST, request.FILES)
        if form.is_valid():
            post = form.save(commit=False)
            post.topic = topic
            post.created_by = request.user
            post.docs = form.cleaned_data.get('docs')
            post.save()
            topic.last_updated = timezone.now()
            topic.save()
            return redirect('topic_posts', tp_pk=topic.cat_g.pk, top_pk=top_pk)
    else:
        form = PostForm()
    return render(request, 'reply_topic.html', {'topic': topic, 'form': form})
"""
def model_form_upload_topic(request, tp_pk):
    cate = get_object_or_404(Category, pk=tp_pk)
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():

            form.save()
            return redirect('new_topic', tp_pk=tp_pk)
    else:
        form = DocumentForm()
    return render(request, 'model_form_upload.html', {
        'form': form
    })

def model_form_upload_post(request, tp_pk, top_pk):
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('topic_posts', tp_pk=tp_pk, top_pk=top_pk)
    else:
        form = DocumentForm()
    return render(request, 'model_form_upload.html', {
        'form': form
    })
"""

@method_decorator(login_required, name='dispatch')
class PostUpdateView(UpdateView):
    model = Post
    fields = ('message', )
    template_name = 'edit_post.html'
    pk_url_kwarg = 'po_pk'
    context_object_name = 'post'

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.filter(created_by=self.request.user)

    def form_valid(self, form):
        post = form.save(commit=False)
        post.updated_by = self.request.user
        post.updated_at = timezone.now()
        post.save()
        return redirect('topic_posts', tp_pk=post.topic.cat_g.pk, top_pk=post.topic.pk)

class CategoryListView(ListView):
    model = Category
    context_object_name = 'cats'
    template_name = 'home.html'

class TopicListView(ListView):
    model = Topic
    context_object_name = 'topics'
    template_name = 'topics.html'
    paginate_by = 20

    def get_context_data(self, **kwargs):
        kwargs['cat_g'] = self.cat_g
        return super().get_context_data(**kwargs)

    def get_queryset(self):
        self.cat_g = get_object_or_404(Category, pk=self.kwargs.get('tp_pk'))
        queryset = self.cat_g.topics.order_by('-last_updated').annotate(replies=Count('posts') - 1)
        return queryset

class PostListView(ListView):
    model = Post
    context_object_name = 'posts'
    template_name = 'topic_posts.html'
    paginate_by = 10

    def get_context_data(self, **kwargs):
        session_key = 'viewed_topic_{}'.format(self.topic.pk)
        if not self.request.session.get(session_key, False):
            self.topic.views += 1
            self.topic.save()
            self.request.session[session_key] = True
        kwargs['topic'] = self.topic
        return super().get_context_data(**kwargs)

    def get_queryset(self):
        self.topic = get_object_or_404(Topic, cat_g__pk=self.kwargs.get('tp_pk'), pk=self.kwargs.get('top_pk'))
        queryset = self.topic.posts.order_by('created_at')
        return queryset
